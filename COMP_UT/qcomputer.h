#ifndef QCOMPUTER_H
#define QCOMPUTER_H
#include <QWidget>
#include <QLineEdit>
#include <QTextEdit>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QDebug>
#include <QPushButton>
#include "controleur.h"
#include "Litterale.h"
#include "LitEntier.h"
#include "LitRationelle.h"
#include "LitReelle.h"
#include "Operateur.h"
#include "OperateurLogique.h"
#include "OperateurNumerique.h"
#include "OperateurPile.h"
#include "Pile.h"

class QComputer : public QWidget
{
    // Macro qui doit apparaitre dans la partie privée de toute classe qui déclare des signaux/slots
    Q_OBJECT
    // Message affiché à l'utilisateur
    QLineEdit *message;
    // Etat de la pile
    QTableWidget *vuePile;
    // Saisie de la commande utilisateur
    QLineEdit *commande;
    // Pile et controleur pour réaliser les calculs
    Pile *pile;
    // controleur
    Controleur *controleur;
    // le nombre de row de PILE
    // function pour creer le button
    QPushButton *creerButton(const QString &text, const char *slot);

public:
    int rowCount;
    Pile *getPile() { return pile; }
    QTableWidget *getVuePile() { return vuePile; }
    explicit QComputer(QWidget *parent = 0);
    ~QComputer()
    {
        delete pile;
        // delete controleur;
    }
    void displayVuePile();

private:
    void calculeAuto();

private slots:
    void onButtonClicked();
    void onSpaceClicked();
    void enterClicked();
    //     // Rafraichir l'affichage de la pile
    void update();

    //     void refresh();
    //     // Interpréter la saisie utilisateur
    //     void getNextCommande();
};
#endif // QCOMPUTER_H
