QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    LitEntier.cpp \
    LitExpression.cpp \
    LitNumerique.cpp \
    LitRationelle.cpp \
    LitReelle.cpp \
    MainWindow.cpp \
    Operateur.cpp \
    OperateurManager.cpp \
    Pile.cpp \
    controleur.cpp \
    main.cpp \
    qcomputer.cpp

HEADERS += \
    CalculatorException.h \
    LitAtom.h \
    LitEntier.h \
    LitExpression.h \
    LitNumber.h \
    LitNumerique.h \
    LitRationelle.h \
    LitReelle.h \
    Litterale.h \
    MainWindow.h \
    Operateur.h \
    OperateurLogique.h \
    OperateurManager.h \
    OperateurNumerique.h \
    OperateurPile.h \
    Pile.h \
    Stockage.h \
    controleur.h \
    qcomputer.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
