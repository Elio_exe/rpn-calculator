#include <QRegExp>
#include "controleur.h"
#include "Stockage.h"

bool Controleur::estOperateur(const QString &c)
{
    Operateur *resultat = OpeManager->operateur_map[c];
    return (resultat != nullptr);
}

Operateur *Controleur::getOperateur(const QString &c)
{
    Operateur *resultat = OpeManager->operateur_map[c];
    if (!resultat)
        throw CalculatorException("Invalide String");
    return resultat;
}

bool Controleur::estLitEntier(const QString &c)
{
    bool flag = false;
    c.toInt(&flag);
    return flag;
}

bool Controleur::estLitRattionelle(const QString &c)
{
    QList<QString> list = c.split('/');
    if (list.size() == 2)
    {
        if (estLitEntier(list[0]) && estLitEntier(list[1]))
        {
            if (list[1].toInt() == 0)
                return false;

            return true;
        }
    }
    return false;
}

bool Controleur::estLitReelle(const QString &c)
{
    bool flag = false;
    c.toDouble(&flag);
    return flag;
}

bool Controleur::estLitterale(const QString &c)
{
    return (estLitEntier(c) || estLitRattionelle(c) || estLitReelle(c) || estLitExpression(c) || estLitAtom(c));
}

Litterale *Controleur::getLitterale(const QString &c)
{
    if (estLitEntier(c))
    {
        return new LitEntier(c.toInt());
    }

    if (estLitRattionelle(c))
    {
        QList<QString> list = c.split('/');
        return new LitRationelle(list[0].toInt(), list[1].toInt());
    }

    if (estLitReelle(c))
    {
        double temp = c.toDouble();
        return new LitReelle(temp);
    }

    if (estLitExpression(c))
    {
        return new LitExpression(c);
    }

    if (estLitAtom(c))
    {
        return OpeManager->stockage->getLitAtom(c);
    }
}

bool Controleur::estLitExpression(const QString &c)
{
    QRegExp exp("^'[A-Z]+[0-9]*[A-Z]*'$");
    return c.contains(exp);
}

bool Controleur::estLitAtom(const QString &c)
{
    QRegExp exp("^[A-Z]+[0-9]*[A-Z]*$");
    return c.contains(exp);
}

void Controleur::traiter(const QString &c)
{
    // split the stringeg
    QList<QString> list = c.split(QRegExp("[\\s\\n\\r]+"));

    for (int i = 0; i < list.length(); i++)
    {
        if (list[i].toStdString().find_first_not_of(' ') == std::string::npos)
        {
            continue;
        }
        // If operand est un litterale
        if (estOperateur(list[i]))
        {
            Operateur *Ope = getOperateur(list[i]);
            // operateur clear
            if (Ope->getSympole() == "CLEAR")
            {
                Ope->traiter(maPile);
            }

            if (Ope->getArite() == 2)
            {
                if (maPile->taille() < 2)
                {
                    throw CalculatorException("C'est impossible a utiliser cette Operateur Binaire");
                }
                // pour des operateur PILE
                if (Ope->getSympole() == "SWAP")
                {
                    Ope->traiter(maPile);
                }
                else
                {
                    // depile n operateurs
                    Litterale *lt2 = maPile->top();
                    maPile->pop();
                    Litterale *lt1 = maPile->top();
                    maPile->pop();
                    // l'empilement le resultat
                    maPile->push(Ope->traiter(lt1, lt2));
                }
            }

            if (Ope->getArite() == 1)
            {
                if (maPile->taille() < 1)
                {
                    throw CalculatorException("C'est impossible a utiliser cette Operateur Binaire");
                }
                // pour des operateur PILE
                if (Ope->getSympole() == "DUP" || Ope->getSympole() == "DROP")
                {
                    Ope->traiter(maPile);
                }
                else
                {
                    // depile n operateurs
                    Litterale *lt1 = maPile->top();
                    maPile->pop();
                    // l'empilement le resultat
                    maPile->push(Ope->traiter(lt1));
                }
            }
        }
        else if (estLitterale(list[i]))
        {
            // eviter litterale Atom comme Operateur
            if (estLitExpression(list[i]) && estOperateur(list[i].mid(1, list[i].length() - 2)))
            {
                throw CalculatorException("Operateur ne peut pas etre utilise comme Litterale Atom");
            }
            // pour autre litterale
            Litterale *temp = getLitterale(list[i]);
            if (temp != nullptr)
            {
                if (estLitAtom(list[i]))
                {
                    maPile->push(temp->getValeurNumerique());
                }
                else
                {
                    maPile->push(temp);
                }
            }
            else
            {
                throw CalculatorException("Atom n'existe pas");
            }
        }
        else
        {
            throw CalculatorException("Impossible pour tapper cette Expression ");
        }
    }
    // QString operateurStr = list[2];
}
