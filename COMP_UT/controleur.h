#ifndef Controleur_h
#define Controleur_h
#include "OperateurManager.h"
#include "Pile.h"
#include "Stockage.h"

class Controleur
{
    Pile *maPile;
    OperateurManager *OpeManager;

public:
    Controleur(OperateurManager &m, Pile &v) : OpeManager(&m), maPile(&v) {}
    Controleur() {}
    bool estOperateur(const QString &c);
    Operateur *getOperateur(const QString &c);

    bool estLitEntier(const QString &c);
    bool estLitRattionelle(const QString &c);
    bool estLitReelle(const QString &c);
    bool estLitterale(const QString &c);
    bool estLitExpression(const QString &c);
    bool estLitAtom(const QString &c);

    Litterale *getLitterale(const QString &c);

    void traiter(const QString &c);

    // bool estLitterale(const QString &c);

    // estUnnombre
};

#endif
