#include "MainWindow.h"
#include <QInputDialog>
void MainWindow::open_Paramtres()
{
    QString text = QInputDialog::getText(this, "Parametres X", "Vous voulez afficher combien des valeurs ? ");
    computer->getVuePile()->setRowCount(text.toInt());
    computer->rowCount = text.toInt();
    int nbRow = computer->getVuePile()->rowCount();
    std::cout << computer->getPile()->taille();
    QStringList labelList;
    for (unsigned int i = nbRow; i > 0; i--)
    {
        QString str = QString::number(i);
        str += " : ";
        labelList << str;
    }
    computer->getVuePile()->setVerticalHeaderLabels(labelList);

    for (unsigned int i = 0; i < nbRow; i++)
    {
        computer->getVuePile()->setItem(i, 0, new QTableWidgetItem());
    }
 
    int count = 1;
    for (Pile::iterator it = computer->getPile()->begin(); (it != computer->getPile()->end()) && (count <= nbRow); ++it)
    {
        computer->getVuePile()->item(nbRow - count, 0)->setText((*it).getText());
        count++;
    }
    for (size_t i = count; i <= computer->getVuePile()->rowCount(); i++)
    {
        computer->getVuePile()->item(nbRow - i, 0)->setText("");
    }
}

MainWindow::MainWindow(QWidget *parent)
{
    computer = new QComputer();
    // Setup File Menu
    compMenu = menuBar()->addMenu("&CompUT");
    quitAction = new QAction("Quit", this);
    paramsAction = new QAction("Parametres", this);
    compMenu->addAction(paramsAction);
    compMenu->addAction(quitAction);
    QMainWindow::setCentralWidget(computer);

    // SET up signal et slots

    // QUIT action
    connect(quitAction, &QAction::triggered, this, &QApplication::quit);
    // Paramettre action
    connect(paramsAction, SIGNAL(triggered()), this, SLOT(open_Paramtres()));
}
